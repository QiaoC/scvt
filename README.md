# Spherical Centroid Voronoi Tessellation (SCVT) for Unit Sphere

The repository contains the source code for generating SCVT on a unit sphere. The program is written by [Dr. Lili Ju](http://people.math.sc.edu/ju/) at USC. The following sections will be structured similarly from the original [readme file](https://bitbucket.org/QiaoC/scvt/src/master/README.ori).

## Introduction

The **SCVT** package is used to generate the *Spherical Centroid Voronoi Tessellation*
[1] on the unit sphere. It is only for educational use.

## Code Structure

It consists of the following files:

* ```scvt_ini.f```  (Generate the inital SCVT generators using Monte Carlo method according to the given density function)
* ```scvt_opt.f```  (Generate the SCVT mesh using Lloyd's method)
* ```grid_ref.f```  (Refine the SVT method by adding middle generators on edges)
* ```draw_diag.f``` (Draw the spherical Voronoi diagram and Delaunay triangulation)
* ```density.f```   (Define the density for generators distribution)
* ```randgen.f```   (Random point generator on the unit sphere)
* ```process.f```   (Process the SVDT information and generate output file)
* ```svtgen.f```    (Spherical Voronoi Tessellation generator for given generators which is a part of "STRIPACK" package [2])
* ```scvt.in```     (input file)
* ```scvt.m```      (variable files)

Sample output generators files: ```scvt_12.dat```, ```scvt_42.dat```, ```scvt_162.dat```, ```scvt_642.dat```, ```scvt_2562.dat```.

## How to Run It

### Step 0: Preprocess

* Define a density function in ```density.f```, see the sample density functions defined there.
* Compile, i.e. ```make```.

### Step 1: Edit the ```scvt.in``` file

* The 2nd row is the number of generators/nodes "n"
* The 4th row is the maximal number of iterations "max_iter" for Lloyd's algorithm.
* The 6th row is the tolerance set for Lloyd's method (eps)
* The 8th row is whether we place one node at the poles? (0-->no  -1-->south  1-->North)
* See the sample ```scvt.in``` file included in the package
* The Stride number means a re-triangulation is done after Stride-steps of iterations.

### Step 2: Generate the initial SCVT generators using Monte Carlo method according to the density function

* invoke ```scvt_ini```
  - It reads ```scvt.in``` for number of generators
  - The output generators are saved in ```scvt_mc.dat```

### Step 3: Generate the SCVT using Lloyd's method

* ```cp scvt_mc.dat scvt_s.dat```
* invoke ```scvt_opt```
  - It reads ```scvt.in``` and takes ```scvt_s.dat``` as the input file for initial positions of generators
  - The output generators are saved in ```scvt_lloyd.dat```
  - The output SCVT mesh information is saved in ```voronoi.dat```
  - The ouput *spherical delaunay triangulation* information is saved in ```deltri.dat```

### Step 4: Draw the SCVT and SDT

* ```cp scvt_lloyd.dat nodes.dat```
* invoke ```draw_diag```
  - It takes ```nodes.dat``` as the input file for positions of generators
  - The output SCVT diagram is saved in ```voronoi.eps```
  - The output SDT diagram is saved in ```deltri.eps```

### Step 5: Refine the spherical meshes

* ```cp scvt_lloyd.dat nodes_s.dat```
* invoke ```grid_ref```
  - It takes ```nodes_s.dat``` as the input file for starting generators
  - The output for ending generators is saved in ```nodes_e.dat```

## References:

* [1] [Q. Du, M. Gunzburger and  L.Ju, Constrained centroidal Voronoi tessellations for surfaces, SIAM J. Sci. Comput. 24 (2003), pp. 1488-1506.](https://epubs.siam.org/doi/abs/10.1137/S1064827501391576)
* [2] [R. Renka, ALGORITHM 772, STRIPACK: Delaunay triangulation and Voronoi diagrams on the surface of a sphere, ACM Trans. Math. Soft. 23 (1997), pp. 416-434.](https://dl.acm.org/citation.cfm?id=275329)
